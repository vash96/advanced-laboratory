# Pangenome

Computational pangenomics is interested in structuring, managing and indexing genomes of all strains in a clade (in our case, we are interested in the genome of a species) in a more meaningful way than as a string of symbols.

# References
1. Cazaux B., Kosolobov D., Mäkinen V., Norri T. (2019) Linear Time Maximum Segmentation Problems in Column Stream Model. In: Brisaboa N., Puglisi S. (eds) String Processing and Information Retrieval. SPIRE 2019. Lecture Notes in Computer Science, vol 11811. Springer, Cham. https://doi.org/10.1007/978-3-030-32686-9_23

2. Mäkinen, V., Cazaux, B., Equi, M., Norri, T., & Tomescu, A. I. (2020). Linear Time Construction of Indexable Founder Block Graphs. CoRR, abs/2005.09342. https://arxiv.org/abs/2005.09342

3. Norri, T., Cazaux, B., Kosolobov, D. et al. Linear time minimum segmentation enables scalable founder reconstruction. Algorithms Mol Biol 14, 12 (2019). https://doi.org/10.1186/s13015-019-0147-6

4. Ukkonen E. (2002) Finding Founder Sequences from a Set of Recombinants. In: Guigó R., Gusfield D. (eds) Algorithms in Bioinformatics. WABI 2002. Lecture Notes in Computer Science, vol 2452. Springer, Berlin, Heidelberg. https://doi.org/10.1007/3-540-45784-4_21