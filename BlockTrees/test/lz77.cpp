#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

using namespace std;

#include "types.c"

const int MAX_WINDOW_SIZE = 0xff;
// const int MAX_WINDOW_SIZE = 6;

pair<int,int> lz77_encode(istream &in, ostream &out) {
  circular_array<char> window(MAX_WINDOW_SIZE * 2);
  int loaded = 0, window_size = 0, bytes = 0, compressed_bytes = 0;

  while(!in.eof() || loaded > 0) {
    // Load the look ahead buffer
    while(loaded < MAX_WINDOW_SIZE && !in.eof()) {
      char c;
      in.get(c);
      if(in.eof()) {
        c = 0;
      }
      window.push_back(c);
      loaded++;
    }

    int match_index = window_size, max_length = 0;
    for(int i = 0; i < window_size; i++) {
      int length = 0;
      int a = window_size, b = i;
      while(length < loaded && a < window_size + loaded && window[a] == window[b]) {
        length++;
        a++;
        b++;
      }

      if(length > max_length) {
        max_length = length;
        match_index = i;
      }
    }

    pointer p = {(uint8_t) (window_size - match_index), (uint8_t) max_length, window[window_size + max_length]};
    // cerr << "(" << (int)p.offset << ", " << (int) p.length << ", " << p.c << ")\n";
    out << p;
    loaded -= max_length + 1;
    bytes += max_length + 1;
    // cerr << bytes << endl;
    compressed_bytes += sizeof(pointer);
    window_size = window_size + max_length + 1;
    if(window_size > MAX_WINDOW_SIZE) {
      window.pop_front(window_size - MAX_WINDOW_SIZE);
      window_size = MAX_WINDOW_SIZE;
    }
  }

  return {bytes, compressed_bytes};
}

void lz77_decode(istream &in, ostream &out) {
  circular_array<char> window(MAX_WINDOW_SIZE * 2);
  while(true) {
    pointer p;
    in >> p;
    if(in.eof()) break;

    // cerr << "(" << (int)p.offset << ", " << (int) p.length << ", " << p.c << ")\n";

    int index = window.size() - p.offset;

    for(int i = 0; i < p.length; i++) {
      out << window[index + i];
      window.push_back(window[index + i]);
    }

    if(p.c != 0) {
      out << p.c;
      window.push_back(p.c);
    }
    if(window.size() > MAX_WINDOW_SIZE) {
      window.pop_front(window.size() - MAX_WINDOW_SIZE);
    }
  }
}

int main() {

  string s = "dfjhhjdfhd";
  cout << s.length() << endl;
  s.clear();
  cout << s.length() << endl;
  return 0;

  auto i = lz77_encode(cin, cout);

  float factor = (float) (i.first - i.second) / (float) i.first * 100.0;

  cerr << "Original size: " << i.first << endl;
  cerr << "Compressed size: " << i.second << endl;
  cerr << "Compression factor: " << factor << "%\n";

  return 0;
}
