template<class T>
class circular_array {
private:
  T* array;
  int max_size;
  int current_size;
  int current_index;
public:
  circular_array(int size) {
    array = (T*) malloc(size * sizeof(T));
    max_size = size;
    current_size = 0;
    current_index = 0;
  }
  ~circular_array() {
    free(array);
  }
  int push_back(const T &x) {
    current_size = min(max_size, current_size + 1);
    array[current_index] = x;
    current_index = (current_index + 1) % max_size;
    return current_index;
  }
  int pop_front() {
    return pop_front(1);
  }
  int pop_front(int k) {
    if(current_size < k) return -1;
    current_size -= k;
    return current_size;
  }
  int size() {
    return current_size;
  }
  T& operator [](const int &i) {
    // int index = (current_size == max_size) ? ((current_index + i) % max_size) : (i % max_size);
    int index = (i + current_index - current_size + max_size) % max_size;
    return array[index];
  }
};

struct pointer {
  uint8_t offset;
  uint8_t length;
  char c;
};

istream &operator >> (istream &in, pointer &p) {
  char a, b, c;
  in.get(a).get(b).get(c);
  p = {(uint8_t) a, (uint8_t) b, c};
  return in;
}


ostream &operator << (ostream &out, const pointer &p) {
  return out << (char) p.offset << (char) p.length << p.c;
}
