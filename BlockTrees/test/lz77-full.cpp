#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <cassert>

using namespace std;

struct pointer {
  uint32_t offset;
  uint32_t length;
  char c;
};

istream &operator >> (istream &in, pointer &p) {
  uint32_t a, b;
  char c;
  a = (in.get() << 24) | (in.get() << 16) | (in.get() << 8) | in.get();
  b = (in.get() << 24) | (in.get() << 16) | (in.get() << 8) | in.get();
  c = in.get();
  p = {a, b, c};
  return in;
}

ostream &operator << (ostream &out, const pointer &p) {
  out << (char) ((p.offset >> 24) & 0xff) << (char) ((p.offset >> 16) & 0xff) << (char) ((p.offset >> 8) & 0xff) << (char) (p.offset & 0xff);
  out << (char) ((p.length >> 24) & 0xff) << (char) ((p.length >> 16) & 0xff) << (char) ((p.length >> 8) & 0xff) << (char) (p.length & 0xff);
  return out << p.c;
}


class suffix_tree {
public:
  string label, suffix;
  unsigned index;
  unordered_map<char, suffix_tree*> children;

  suffix_tree(string s, unsigned i): label(s), index(i) {
    suffix = "";
  }
  ~suffix_tree() {
    for(auto& it : children) {
      delete it.second;
    }
    children.clear();
    label.clear();
    suffix.clear();
  };

  void add(const string& s, unsigned index) {
    if(suffix.length() > 0) {
      // Propagate commmon suffix
      for(auto& i : children) {
        i.second->suffix.append(suffix);
      }
      suffix.clear();
    }
    if(s.length() == 0) {
      // Check empty suffix
      if(children.find(0) == children.end()) {
        children.insert({0, new suffix_tree("", index)});
      }
    }
    else {
      // Non empty suffix
      auto it = children.find(s[0]);
      if(it != children.end()) {
        // Char exists: remove common prefix and recurse
        int i = 0;
        auto st = it->second;
        while(i < st->label.length() && i < s.length() && st->label[i] == s[i]) {
          i++;
        }
        assert(i <= st->label.length() && i <= s.length());
        if(i < st->label.length()) {
          // st->label is not a prefix of s: split st so it is
          st->split(i);
        }
        // Recurse
        st->add(s.substr(i), index);
      }
      else {
        // New char: add new children
        children.insert({s[0], new suffix_tree(s, index)});
      }
    }
  }

  void append(const string& s, unsigned index) {
    assert(s.length() > 0);
    for(auto& i : children) {
      // Append common suffix to existing ones
      i.second->suffix.append(s);
    }
    for(int i = 0; i < s.length(); i++) {
      // Add the new suffixes
      add(s.substr(i), index + i);
    }
  }

  void split(unsigned i) {
    suffix_tree* st = new suffix_tree(label.substr(i), index);
    st->children = move(children);
    children.clear();
    children.insert({label[i], st});
    label.erase(i);
  }
};

int lz77_encode(istream& in, ostream& out) {
  suffix_tree dictionary("", 0);
  int i = 0;
  int Z = 0;
  bool flag;

  while(in.peek() != EOF) {
    suffix_tree* pt = &dictionary;
    int str_index = 0;
    ostringstream ss;
    char c;

    flag = true;

    while(flag && !in.eof()) {
      c = in.get();
      if(str_index == pt->label.length()) {
        // End of the label: search the child
        auto it = pt->children.find(c);
        if(it == pt->children.end()) {
          // Substring doesn't exists: stop and create new one
          flag = false;
        }
        else {
          // Substring does exist: go to child
          pt = it->second;
          str_index = 0;
        }
      }
      // There are still characters in the label: try to match them
      else if(pt->label[str_index] != c) {
        // No match: stop
        flag = false;
      }
      // If it's a match do nothig, just keep going
      str_index ++;
      ss.put(c);
    }

    Z++;
    string s = ss.str();
    pointer p = {pt->index, (unsigned) (s.length() - 1), c};
    out << p;
    dictionary.append(s, i);
    i += s.length();
    cerr << i << endl;
    if(i > 120000) return Z;
  }

  return Z;
}

void lz77_decode(istream& in, ostream& out) {
  stringstream ss;
  pointer p;

  while(in.peek() != EOF) {
    in >> p;
    printf("%d %d %c\n", p.offset, p.length, p.c);
    ss.seekg(p.offset, ios_base::beg);
    char s[p.length + 1];
    ss.read(s, p.length);
    s[p.length] = '\0';
    ss.seekp(0, ios_base::end);
    ss << s;
    if(p.c != EOF) {
      ss << p.c;
    }
    // out << s;
    // if(p.c != EOF) {
    //   out << p.c;
    // }
  }

  ss.seekp(0);
  ss.seekg(0);
  out << ss.rdbuf();
}

int main() {

  // string s = "AABABBBABAABABBBABBABB";
  // stringstream ss(s);

  ifstream in("../BlockTrees/build/einstein.de.txt");
  ofstream out("output1.z77");

  // lz77_encode(ss, cout);
  cout << lz77_encode(in, out) << endl;
  out.close();
  // out.open("prova.txt");
  // ifstream in("output1.z77");
  // lz77_decode(in, out);

  return 0;
}
